var settings = {
	"windowTitle" : "Clippers Playoffs 2013 | Clippers vs Grizzlies",
	"opponent" : "Grizzlies",
	"subtitle" : "Game 1: Sat. 7:30pm PRIME / ESPN",
	"clippersTally" : 0,
	"opponentTally" : 0,
	"numSlides" : 17,
	"baseImageName" : "CLP13-Game0-Images",
	"baseImageURL" : "http://heilbrice.com/clients/clippers/2013/clippers-playoffs-2013/game-images/"
};



$(document).ready(function(){
	init();
});

function init() {
	rewriteWindowTitle();
	fillInTitles();
	addSlides();
	adjustSlideShowSize();
	registerAudioSwitch();
	playMusic();

	if(Modernizr.cssanimations) {
		setTimeout(function(){
			startSlideshow();
		},4000);
	} else {
		startSlideshow();
	}
	
}

function rewriteWindowTitle() {
	document.title = settings.windowTitle;
}

function addSlides() {
	var baseImagePath = settings.baseImageURL + settings.baseImageName;
	$('.sequence-canvas').html('');

	
	for( var index = 0; index < settings.numSlides; index++){
		var src = '',
			$li = $('<li />'),
			$img = $('<img class="model" />');

		// first file does not have number
		if(index == 0) {
			src = baseImagePath + '.jpg';
			$li.addClass('animate-in');
		} else {
			src = baseImagePath + index + '.jpg';
		}

		$img.attr('src', src).appendTo($li);

		$('.sequence-canvas').append($li);
	}

}

function adjustSlideShowSize() {
	$(window).resize(function(){
		var aspect = 820 / 520,
		$canvas = $('#sequence'),
		canvasWidth = $canvas.width();

		if(canvasWidth > 820) {
			return;
		}

		$canvas.height(canvasWidth / aspect);

	}).trigger('resize');
	
}

function fillInTitles() {
	$('#score .home .tally').html(settings.clippersTally);
	$('#score .opponent .tally').html(settings.opponentTally);
	$('#title-container h2').html(settings.subtitle);
	$('#title-container .opponent').html(settings.opponent);
}

function startSlideshow() {
    var options = {
        nextButton: false,
        prevButton: false,
        pagination: false,
        animateStartingFrameIn: true,
        autoPlay: true,
        autoPlayDelay: 2000,
        cycle: false
    };
    
    var mySequence = $("#sequence").sequence(options).data("sequence");

    mySequence.afterNextFrameAnimatesIn = function(){
	    var frameID = mySequence.currentFrameID;

	    goToClippersHome(frameID);
    };

    mySequence.afterLoaded = function(){
    	playMusic();
    }
}

function goToClippersHome(frameID){
	// if this is the last frame, then go to clippers home page
	if(frameID == settings.numSlides) {

		window.setInterval(function(){
			window.location = "http://clippers.com";
		},3000);
	}
}

function playMusic() {
	var song = $('#music').get(0);
	song.play();
}

function stopMusic() {
	var song = $('#music').get(0);
	song.pause();	
}

function registerAudioSwitch() {
	$('#audioSwitch').click(function(){
		$(this).toggleClass('off');

		if($(this).hasClass('off')) {
			stopMusic();
		} else {
			playMusic();
		}
	});
}